package com.example.emailrelay;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import com.example.emailrelay.filtering.EmailFilter;
import com.example.emailrelay.filtering.HtmlTagsEmailBodyFilter;
import com.example.emailrelay.forwarder.EmailForwarderManager;
import com.example.emailrelay.health.LocalQueueSizeHealthCheck;
import com.example.emailrelay.persistence.EmailEventStore;
import com.example.emailrelay.persistence.EmailStore;
import com.example.emailrelay.persistence.LocalEmailEventStore;
import com.example.emailrelay.persistence.LocalEmailStore;
import com.example.emailrelay.queuing.LocalQueueParcelReceiveHandler;
import com.example.emailrelay.queuing.LocalQueueParcelReceiveHandlerPool;
import com.example.emailrelay.queuing.LocalQueueParcelSendHandler;
import com.example.emailrelay.queuing.LocalSharedQueue;
import com.example.emailrelay.queuing.ParcelReceiveHandler;
import com.example.emailrelay.queuing.ParcelReceiveHandlerPool;
import com.example.emailrelay.queuing.ParcelSendHandler;
import com.example.emailrelay.resources.EmailEventResource;
import com.example.emailrelay.resources.EmailResource;
import com.example.emailrelay.resources.ReceiveHandlerPoolResource;
import com.example.emailrelay.resources.ReceiveStatisticsResource;
import com.example.emailrelay.resources.SearchResource;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;

public final class EmailRelayApplication extends Application<EmailRelayConfiguration> {

	public static void main(String[] args) throws Exception {
		new EmailRelayApplication().run(args);
	}

	@Override
	public String getName() {
		return "email-relay";
	}

	@Override
	public void initialize(Bootstrap<EmailRelayConfiguration> bootstrap) {
	}

	@Override
	public void run(EmailRelayConfiguration configuration, Environment environment) throws Exception {

		final Injector injector = createInjector(configuration);

		// Start receivers.
		injector.getInstance(ParcelReceiveHandlerPool.class);

		environment.jersey().register(injector.getInstance(EmailResource.class));
		environment.jersey().register(injector.getInstance(ReceiveHandlerPoolResource.class));
		environment.jersey().register(injector.getInstance(ReceiveStatisticsResource.class));
		environment.jersey().register(injector.getInstance(SearchResource.class));
		environment.jersey().register(injector.getInstance(EmailEventResource.class));

		environment.healthChecks().register("EmailQueueSize", injector.getInstance(LocalQueueSizeHealthCheck.class));
	}

	private Injector createInjector(final EmailRelayConfiguration conf) {
		return Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {
				bind(EmailRelayConfiguration.class).toInstance(conf);

				// In case of separation of producers and consumers into
				// different processes, this has to go to consumer.
				bind(EmailFilter.class).toInstance(new HtmlTagsEmailBodyFilter(null));

				// Setup things for use of the local queue. Later it could be something more sophisticated.
				// Since we want all handlers to share the queue, make it a singleton.
				bind(LocalSharedQueue.class).in(Scopes.SINGLETON);
				bind(EmailStore.class).to(LocalEmailStore.class).in(Scopes.SINGLETON);
				bind(EmailEventStore.class).to(LocalEmailEventStore.class).in(Scopes.SINGLETON);
				bind(EmailForwarderManager.class).in(Scopes.SINGLETON);
				bind(ParcelSendHandler.class).to(LocalQueueParcelSendHandler.class);
				bind(ParcelReceiveHandler.class).to(LocalQueueParcelReceiveHandler.class);
				bind(ParcelReceiveHandlerPool.class).to(LocalQueueParcelReceiveHandlerPool.class).in(Scopes.SINGLETON);
			}
		});
	}
}
