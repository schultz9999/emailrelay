package com.example.emailrelay;

import javax.validation.constraints.NotNull;

import io.dropwizard.Configuration;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import com.example.emailrelay.api.EmailServiceVendor;

public class EmailRelayConfiguration extends Configuration {
	@Range(min = 1, max = 100)
	private int receiveWorkerCount;
	
	@NotEmpty
	private String mailgunApiKey;
	@NotEmpty
	private String mailgunApiUrl;

	@NotEmpty
	private String sendGridApiUser;
	@NotEmpty
	private String sendGridApiKey;
	@NotEmpty
	private String sendGridApiUrl;
	
	@NotNull
	private EmailServiceVendor primaryEmailServiceVendor;
	@NotNull
	private EmailServiceVendor secondaryEmailServiceVendor;


	
	public int getReceiveWorkerCount() {
		return receiveWorkerCount;
	}

	public void setReceiveWorkerCount(int receiveWorkerCount) {
		this.receiveWorkerCount = receiveWorkerCount;
	}

	public String getMailgunApiKey() {
		return mailgunApiKey;
	}

	public void setMailgunApiKey(String mailgunApiKey) {
		this.mailgunApiKey = mailgunApiKey;
	}

	public String getMailgunApiUrl() {
		return mailgunApiUrl;
	}

	public void setMailgunApiUrl(String mailgunApiUrl) {
		this.mailgunApiUrl = mailgunApiUrl;
	}

	public String getSendGridApiUser() {
		return sendGridApiUser;
	}

	public void setSendGridApiUser(String sendGridApiUser) {
		this.sendGridApiUser = sendGridApiUser;
	}

	public String getSendGridApiKey() {
		return sendGridApiKey;
	}

	public void setSendGridApiKey(String sendGridApiKey) {
		this.sendGridApiKey = sendGridApiKey;
	}

	public String getSendGridApiUrl() {
		return sendGridApiUrl;
	}

	public void setSendGridApiUrl(String sendGridApiUrl) {
		this.sendGridApiUrl = sendGridApiUrl;
	}

	public EmailServiceVendor getPrimaryEmailServiceVendor() {
		return primaryEmailServiceVendor;
	}

	public void setPrimaryEmailServiceVendor(EmailServiceVendor primaryEmailServiceVendor) {
		this.primaryEmailServiceVendor = primaryEmailServiceVendor;
	}

	public EmailServiceVendor getSecondaryEmailServiceVendor() {
		return secondaryEmailServiceVendor;
	}

	public void setSecondaryEmailServiceVendor(EmailServiceVendor secondaryEmailServiceVendor) {
		this.secondaryEmailServiceVendor = secondaryEmailServiceVendor;
	}
}
