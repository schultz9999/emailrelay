package com.example.emailrelay.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class Email implements Serializable {
	private static final long serialVersionUID = -3319852988716774289L;
	
	private String to;
	private String toName;
	private String from;
	private String fromName;
	private String subject;
	private String body;

	@JsonProperty(value = "to", required = true)
	@org.hibernate.validator.constraints.Email
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@JsonProperty(value = "to_name", required = true)
	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	@JsonProperty(value = "from", required = true)
	@org.hibernate.validator.constraints.Email
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@JsonProperty(value = "from_name", required = true)
	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	@JsonProperty(value = "subject", required = true)
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@JsonProperty(value = "body", required = true)
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	// For json serialization.
	@SuppressWarnings("unused")
	private Email() {
	}

	public Email(String from, String fromName, String to, String toName,
			String subject, String body) {
		this.from = from;
		this.fromName = fromName;
		this.to = to;
		this.toName = toName;
		this.subject = subject;
		this.body = body;
	}

	@Override
	public String toString() {
		return new StringBuilder("From:").append(this.fromName).append("(")
				.append(this.from).append(")").append("\nTo:")
				.append(this.toName).append("(").append(this.to).append(")")
				.append("\nSubject:").append(this.subject).append("\nBody:")
				.append(this.body).toString();

	}
}
