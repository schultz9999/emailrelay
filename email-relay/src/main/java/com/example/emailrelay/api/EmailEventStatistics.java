package com.example.emailrelay.api;

import java.util.HashMap;
import java.util.Map;

public class EmailEventStatistics {
	private Map<String, Integer> eventStats = new HashMap<String, Integer>();

	public Map<String, Integer> getEventStats() {
		return eventStats;
	}

	public void setEventStats(Map<String, Integer> eventStats) {
		this.eventStats = eventStats;
	}
}
