package com.example.emailrelay.api;

public final class EmailResponse {
	
	private String message;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
	// For json serializer.
	@SuppressWarnings("unused")
	private EmailResponse() {}
	
	public EmailResponse(int code, String message) {
		this.message = message;
	}
}
