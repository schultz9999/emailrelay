package com.example.emailrelay.api;

import java.util.List;

import javax.validation.constraints.NotNull;

public final class EmailSearchRequest {
	@NotNull
	private List<String> keywords;

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

}
