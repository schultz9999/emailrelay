package com.example.emailrelay.api;

public enum EmailServiceVendor {
	MAILGUN,
	SENDGRID
}
