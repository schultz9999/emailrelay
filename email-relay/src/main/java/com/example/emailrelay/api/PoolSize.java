package com.example.emailrelay.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PoolSize {
	private int size;

	@JsonProperty(required = true)
	@org.hibernate.validator.constraints.Range(min = 1, max = 100)
	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@SuppressWarnings("unused")
	private PoolSize() {
	}

	public PoolSize(int size) {
		this.size = size;
	}
}
