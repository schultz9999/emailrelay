package com.example.emailrelay.api;

import java.util.Date;
import java.util.UUID;

public final class ReceiveStatistics {
	private String id;
	private Date startedUtc;
	private int count;
	private Date lastReceivedUtc;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getStartedUtc() {
		return startedUtc;
	}

	public void setStartedUtc(Date startedUtc) {
		this.startedUtc = startedUtc;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getLastReceivedUtc() {
		return lastReceivedUtc;
	}

	public ReceiveStatistics() {
		this.startedUtc = new Date();
		this.id = UUID.randomUUID().toString();
	}

	public void setLastReceivedUtc(Date lastReceivedUtc) {
		this.lastReceivedUtc = lastReceivedUtc;
	}

	public void incrementCount() {
		this.count++;
		lastReceivedUtc = new Date();
	}
}
