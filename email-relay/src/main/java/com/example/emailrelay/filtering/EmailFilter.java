package com.example.emailrelay.filtering;

import com.example.emailrelay.api.Email;

public interface EmailFilter {
	void filter(Email email);
}
