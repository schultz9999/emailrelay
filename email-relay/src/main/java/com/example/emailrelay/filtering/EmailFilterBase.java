package com.example.emailrelay.filtering;

import com.example.emailrelay.api.Email;

public abstract class EmailFilterBase implements EmailFilter {

	private final EmailFilter next;

	protected EmailFilterBase(EmailFilter next) {
		this.next = next;
		
	}
	
	public final void filter(Email email) {
		apply(email);
		if (next != null) {
			next.filter(email);
		}
	}
	
	protected abstract void apply(Email email);
}
