package com.example.emailrelay.filtering;

import com.example.emailrelay.api.Email;


public final class HtmlTagsEmailBodyFilter extends EmailFilterBase {

	public HtmlTagsEmailBodyFilter(EmailFilter next) {
		super(next);
		// TODO Auto-generated constructor stub
	}

	protected void apply(Email email) {
		email.setBody(email.getBody().replaceAll("<[^>]*>", " ").replaceAll("\\s+", " ").trim());
	}
}
