package com.example.emailrelay.forwarder;

import com.example.emailrelay.api.Email;

public interface EmailForwarder {

	boolean sendMessage(Email email);
	EmailForwarderStatistics stats();

}