package com.example.emailrelay.forwarder;

import com.example.emailrelay.api.EmailServiceVendor;

abstract class EmailForwarderBase implements EmailForwarder {

	protected final EmailForwarderStatistics stats;

	protected EmailForwarderBase(EmailServiceVendor emailServiceVendor) {
		this.stats = new EmailForwarderStatistics();
		this.stats.setVendor(emailServiceVendor);
	}

	public EmailForwarderStatistics stats() {
		return this.stats;
	}
}
