package com.example.emailrelay.forwarder;

import java.util.Arrays;
import java.util.List;

import com.example.emailrelay.EmailRelayConfiguration;
import com.example.emailrelay.api.Email;
import com.example.emailrelay.api.EmailServiceVendor;
import com.google.inject.Inject;

public final class EmailForwarderManager {

	private final EmailRelayConfiguration config;
	private final EmailForwarder primary;
	private final EmailForwarder secondary;

	@Inject
	public EmailForwarderManager(EmailRelayConfiguration config) {
		this.config = config;

		// TODO (ds): we can get sophisticated here... but we won't for now.
		this.primary = createForwarder(this.config.getPrimaryEmailServiceVendor());
		this.secondary = createForwarder(this.config.getSecondaryEmailServiceVendor());
	}

	private EmailForwarder createForwarder(EmailServiceVendor emailServiceVendor) {
		
		if (emailServiceVendor == null) {
			return null;
		}
		
		switch (emailServiceVendor) {
		case MAILGUN:
			return new MailgunEmailForwarder(this.config);
		case SENDGRID:
			return new SendGridEmailForwarder(this.config);
		default:
			return null;
		}
	}

	public boolean sendMessage(Email email) {
		// Try primary first and then secondary in case of failures.
		if (!this.primary.sendMessage(email)) {
			return this.secondary.sendMessage(email);
		}

		return true;
	}

	public List<EmailForwarderStatistics> stats() {
		// TODO (ds): returning object directly may result some changes before they are used.
		// But we are not looking for perfection in numbers here, just general sense.
		return Arrays.asList(this.primary.stats(), this.secondary.stats()); 
	}
}
