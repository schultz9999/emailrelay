package com.example.emailrelay.forwarder;

import java.util.Date;

import com.example.emailrelay.api.EmailServiceVendor;

public class EmailForwarderStatistics {
	private EmailServiceVendor vendor;
	private int atteptedCount;
	private int successCount;
	private Date lastSuccessUtc;
	private int failedCount;
	private Date lastFailedUtc;

	public EmailServiceVendor getVendor() {
		return this.vendor;
	}

	public void setVendor(EmailServiceVendor vendor) {
		this.vendor = vendor;
	}

	public int getAtteptedCount() {
		return atteptedCount;
	}

	public void setAtteptedCount(int atteptedCount) {
		this.atteptedCount = atteptedCount;
	}

	public int getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}

	public int getFailedCount() {
		return failedCount;
	}

	public void setFailedCount(int failedCount) {
		this.failedCount = failedCount;
	}

	public Date getLastSuccessUtc() {
		return this.lastSuccessUtc;
	}

	public Date getLastFailedUtc() {
		return this.lastFailedUtc;
	}

	public void reportAttempt() {
		this.atteptedCount++;
	}

	public void reportSuccess() {
		this.successCount++;
		this.lastSuccessUtc = new Date();
	}

	public void reportFailure() {
		this.failedCount++;
		this.lastFailedUtc = new Date();
	}
}
