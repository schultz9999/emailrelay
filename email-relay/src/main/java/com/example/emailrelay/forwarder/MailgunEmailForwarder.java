package com.example.emailrelay.forwarder;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.emailrelay.EmailRelayConfiguration;
import com.example.emailrelay.api.Email;
import com.example.emailrelay.api.EmailServiceVendor;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

class MailgunEmailForwarder extends EmailForwarderBase {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailgunEmailForwarder.class);
	
	private final WebResource webResource;

	public MailgunEmailForwarder(EmailRelayConfiguration config) {
		super(EmailServiceVendor.MAILGUN);
		
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("api", config.getMailgunApiKey()));
		this.webResource = client.resource(config.getMailgunApiUrl());
	}
	
	public boolean sendMessage(Email email) {
		LOGGER.debug("Forwarding to Mailgun...");
		
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		formData.add("from", String.format("%s <%s>", email.getFromName(), email.getFrom()));
		formData.add("to", String.format("%s <%s>", email.getToName(), email.getTo()));
		formData.add("subject", email.getSubject());
		formData.add("text", email.getBody());

		try {
			stats.reportAttempt();
			ClientResponse response = this.webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
			if (response.getStatus() == 200) {
				stats.reportSuccess();
			}
			return response.getStatus() == 200;
		} catch (Exception e) {
			stats.reportFailure();
			LOGGER.error("Failure while sending email using Mailgun service.", e);
			return false;
		}
	}
}
 