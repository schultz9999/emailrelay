package com.example.emailrelay.forwarder;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.emailrelay.EmailRelayConfiguration;
import com.example.emailrelay.api.Email;
import com.example.emailrelay.api.EmailServiceVendor;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

final class SendGridEmailForwarder extends EmailForwarderBase {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendGridEmailForwarder.class);
	private final WebResource webResource;
	private final EmailRelayConfiguration config;
			
	protected SendGridEmailForwarder(EmailRelayConfiguration config) {
		super(EmailServiceVendor.SENDGRID);
		this.config = config;
		
		Client client = Client.create();
		this.webResource = client.resource(config.getSendGridApiUrl());
	}

	public boolean sendMessage(Email email) {
		LOGGER.debug("Forwarding to SendGrid...");
		
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		formData.add("api_user", this.config.getSendGridApiUser());
		formData.add("api_key", this.config.getSendGridApiKey());
		formData.add("from", email.getFrom());
		formData.add("fromname", email.getFromName());
		formData.add("to[]", email.getTo());
		formData.add("toname[]", email.getToName());
		formData.add("subject", email.getSubject());
		formData.add("text", email.getBody());

		try {
			stats.reportAttempt();
			ClientResponse response = this.webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
			if (response.getStatus() == 200) {
				stats.reportSuccess();
			}
			return response.getStatus() == 200;
		} catch (Exception e) {
			stats.reportFailure();
			LOGGER.error("Failure while sending email using SendGrid service.", e);
			return false;
		}
	}

}
