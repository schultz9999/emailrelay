package com.example.emailrelay.health;

import com.codahale.metrics.health.HealthCheck;
import com.example.emailrelay.queuing.LocalSharedQueue;
import com.google.inject.Inject;

public final class LocalQueueSizeHealthCheck extends HealthCheck {

	private final LocalSharedQueue queue;

	@Inject
	public LocalQueueSizeHealthCheck(LocalSharedQueue queue) {
		this.queue = queue;
	}

	@Override
	protected Result check() throws Exception {
		final int qs = queue.getQueueSize();
		if (qs < 100) {
			return Result.healthy();
		} else if (qs < 1000) {
			// TODO (ds): pretty artificial number but for the sake of example it should do.
			return Result.healthy("Warning: queue is growing.");
		} else {
			return Result.unhealthy("Queue is too long. Consider adding more receivers. Current queue length: " + qs);
		}
	}

}
