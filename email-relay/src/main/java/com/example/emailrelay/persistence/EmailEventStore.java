package com.example.emailrelay.persistence;

import java.util.List;
import java.util.Map;

import com.example.emailrelay.api.EmailEventStatistics;

public interface EmailEventStore {

	void persist(List<Map<String, String>> events);
	EmailEventStatistics stats();

}
