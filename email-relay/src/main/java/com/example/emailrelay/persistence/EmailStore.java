package com.example.emailrelay.persistence;

import java.util.List;

import com.example.emailrelay.api.Email;

public interface EmailStore {
	void persist(Email email);
	List<Email> search(List<String> keywords);
}
