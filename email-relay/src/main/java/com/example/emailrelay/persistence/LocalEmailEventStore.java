package com.example.emailrelay.persistence;

import java.util.List;
import java.util.Map;

import com.example.emailrelay.api.EmailEventStatistics;

public final class LocalEmailEventStore implements EmailEventStore {

	private static final String UNKNOWN_EVENT = "unknown";

	private final EmailEventStatistics stats = new EmailEventStatistics();
	
	// TODO (ds): not really persists anything -- just creates some stats to return later.
	public void persist(List<Map<String, String>> events) {
		// TODO (ds): pretty bad because serializes all requests. Queuing and then processing later would be better.
		// Otherwise we will not keep up.
		synchronized (stats.getEventStats()) {
			for (Map<String, String> bag : events) {
				String event = bag.getOrDefault("event", UNKNOWN_EVENT).toLowerCase();
				Integer curr = stats.getEventStats().getOrDefault(event, 0);
				stats.getEventStats().put(event, ++curr);
			}
		}
	}

	public EmailEventStatistics stats() {
		return this.stats;
	}

}
