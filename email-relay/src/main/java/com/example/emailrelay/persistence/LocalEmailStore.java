package com.example.emailrelay.persistence;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.example.emailrelay.api.Email;

public final class LocalEmailStore implements EmailStore {

	private final ConcurrentMap<String, Email> store = new ConcurrentHashMap<String, Email>();
	
	
	public void persist(Email email) {
		store.put(email.toString().toLowerCase(), email);
	}

	public List<Email> search(List<String> keywords) {
		List<Email> result = new LinkedList<Email>();
		
		if (keywords ==  null || keywords.size() == 0) {
			return result;
		}
		
		for (String key : store.keySet()) {
			boolean matches = true;
			for (String keyword : keywords) {
				if (!key.contains(keyword.toLowerCase())) {
					matches = false;
					break;
				}
			}
			if (matches) {
				result.add(store.get(key));
			}
		}
		
		return result;
	}
}
