package com.example.emailrelay.queuing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.emailrelay.api.ReceiveStatistics;
import com.example.emailrelay.filtering.EmailFilter;
import com.example.emailrelay.forwarder.EmailForwarderManager;
import com.example.emailrelay.persistence.EmailStore;
import com.example.emailrelay.queuing.LocalSharedQueue.DequeueException;
import com.google.inject.Inject;

public class LocalQueueParcelReceiveHandler implements Runnable, ParcelReceiveHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocalQueueParcelReceiveHandler.class);

	private final LocalSharedQueue queue;
	private final EmailForwarderManager forwarderManager;
	private final ReceiveStatistics stats;
	private final Thread worker;
	private boolean running = true;
	private final EmailStore store;
	private final EmailFilter filter;

	@Inject
	public LocalQueueParcelReceiveHandler(LocalSharedQueue queue, EmailForwarderManager forwarderManager, EmailStore store, EmailFilter filter) {
		this.queue = queue;
		this.forwarderManager = forwarderManager;
		this.store = store;
		this.filter = filter;
		this.stats = new ReceiveStatistics();

		LOGGER.info("Starting worker {}...", this.stats.getId());

		this.worker = new Thread(this);
		// Set to user thread to make sure we are not blocking JVM from exiting.
		this.worker.setDaemon(true);
		this.worker.start();
	}

	public void run() {
		while (this.running) {
			receiveParcel();
		}

		LOGGER.info("Worker {} existed. Parcels processed: {}", this.stats.getId(), this.stats.getCount());
	}

	public void receiveParcel() {
		Parcel parcel;
		try {
			parcel = this.queue.dequeue();

			// This is possible because we are polling.
			if (parcel == null) {
				return;
			}

			this.stats.incrementCount();

			this.store.persist(parcel.getEmail());
			
			this.filter.filter(parcel.getEmail());
			
			this.forwarderManager.sendMessage(parcel.getEmail());

		} catch (DequeueException e) {
			LOGGER.error("Failed to receive a parcel.", e);
		}
	}

	public ReceiveStatistics stats() {
		return this.stats;
	}

	public void shutdown() {
		this.running = false;
	}
}
