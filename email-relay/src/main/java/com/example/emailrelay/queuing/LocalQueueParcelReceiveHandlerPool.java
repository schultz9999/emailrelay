package com.example.emailrelay.queuing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.emailrelay.EmailRelayConfiguration;
import com.example.emailrelay.api.ReceiveStatistics;
import com.example.emailrelay.filtering.EmailFilter;
import com.example.emailrelay.forwarder.EmailForwarderManager;
import com.example.emailrelay.persistence.EmailStore;
import com.google.inject.Inject;

public final class LocalQueueParcelReceiveHandlerPool implements ParcelReceiveHandlerPool {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocalQueueParcelReceiveHandlerPool.class);

	// Use linked list for constant resize performance.
	private final List<LocalQueueParcelReceiveHandler> pool = new LinkedList<LocalQueueParcelReceiveHandler>();
	private final LocalSharedQueue queue;
	private final EmailForwarderManager forwarderManager;
	private final EmailStore store;
	private final EmailFilter filter;

	@Inject
	public LocalQueueParcelReceiveHandlerPool(
			EmailRelayConfiguration config, 
			LocalSharedQueue queue, 
			EmailForwarderManager forwarderManager, 
			EmailStore store,
			EmailFilter filter) {
		this.queue = queue;
		this.forwarderManager = forwarderManager;
		this.store = store;
		this.filter = filter;

		LOGGER.info("Initializing {} parcel receivers...", config.getReceiveWorkerCount());

		for (int i = 0; i < config.getReceiveWorkerCount(); i++) {
			this.pool.add(new LocalQueueParcelReceiveHandler(queue, forwarderManager, store, filter));
		}
	}

	public void resize(int newSize) {

		while (this.pool.size() > newSize) {
			this.pool.get(0).shutdown();
			this.pool.remove(0);
		}

		while (this.pool.size() < newSize) {
			this.pool.add(new LocalQueueParcelReceiveHandler(this.queue, this.forwarderManager, this.store, this.filter));
		}

		LOGGER.info("New receiver pool size: {}", this.pool.size());
	}

	public int size() {
		return this.pool.size();
	}

	public List<ReceiveStatistics> stats() {
		ArrayList<ReceiveStatistics> stats = new ArrayList<ReceiveStatistics>(this.pool.size());
		for (LocalQueueParcelReceiveHandler h : this.pool) {
			stats.add(h.stats());
		}
		return stats;
	}
}
