package com.example.emailrelay.queuing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public final class LocalQueueParcelSendHandler implements ParcelSendHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParcelSendHandler.class);
	
	private final LocalSharedQueue queueProvider;

	@Inject
	public LocalQueueParcelSendHandler(LocalSharedQueue queueProvider) {
		this.queueProvider = queueProvider;

	}

	public void sendParcel(Parcel parcel) {
		try {
			this.queueProvider.enqueue(parcel);
		} catch (Exception e) {
			// TODO (ds): we have a choice here to re-throw and let the user know or retry or ignore.
			// Will ignore for now.
			LOGGER.error("Failed", e);
		}
	}
}
