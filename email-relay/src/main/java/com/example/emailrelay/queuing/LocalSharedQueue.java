package com.example.emailrelay.queuing;

import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.ConcurrentArrayBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LocalSharedQueue {
	private static final Logger LOGGER = LoggerFactory.getLogger(LocalSharedQueue.class);
	
	private final ConcurrentArrayBlockingQueue<Parcel> queue = new ConcurrentArrayBlockingQueue.Unbounded<Parcel>();
	
	@SuppressWarnings("serial")
	public static class EnqueueException extends Exception {
		public EnqueueException(Throwable t) {
			super(t);
		}
	}
	
	@SuppressWarnings("serial")
	public static class DequeueException extends Exception {
		public DequeueException(Throwable t) {
			super(t);
		}
	}
	
	public void enqueue(Parcel parcel) throws EnqueueException {
		try {
			queue.put(parcel);
		} catch (InterruptedException e) {
			LOGGER.error("Interrupted while enqueuing.", e);
			throw new EnqueueException(e);
		}
	}
	
	public Parcel dequeue() throws DequeueException {
		try {
			return queue.poll(1000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			LOGGER.error("Interrupted while dequeuing.", e);
			throw new DequeueException(e);
		}
	}
	
	public int getQueueSize() {
		return queue.size();
	}
}
