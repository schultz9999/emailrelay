package com.example.emailrelay.queuing;

import java.io.Serializable;
import java.util.Date;

import com.example.emailrelay.api.Email;

// TODO (ds): not going to do much at this point but providing separation of Email class 
// from one we put on queue can give us additional flexibility in future.
// Alternatively it can be implemented as a map with string to object mapping.
// Objects must be serializable for objects to be used with generic queuing services.
// For monitoring purposes it may also include some sort of meta data, 
// e.g. created UTC time to estimate how long it took to deliver a parcel.

public class Parcel implements Serializable {
	private static final long serialVersionUID = -5211632896113684730L;
	
	private Email email;
	private Date createdUtc;

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}
	
	public Date getCreatedUtc() {
		return createdUtc;
	}

	public void setCreatedUtc(Date createdUtc) {
		this.createdUtc = createdUtc;
	}

	@SuppressWarnings("unused")
	private Parcel() {
	}
	
	public Parcel(Email email) {
		this.email = email;
		this.createdUtc = new Date();
	}
}
