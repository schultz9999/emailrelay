package com.example.emailrelay.queuing;

import com.example.emailrelay.api.ReceiveStatistics;

public interface ParcelReceiveHandler {
	void receiveParcel();
	ReceiveStatistics stats();
}
