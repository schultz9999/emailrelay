package com.example.emailrelay.queuing;

import java.util.List;

import com.example.emailrelay.api.ReceiveStatistics;

public interface ParcelReceiveHandlerPool {

	void resize(int newSize);
	int size();
	List<ReceiveStatistics> stats();
	
}
