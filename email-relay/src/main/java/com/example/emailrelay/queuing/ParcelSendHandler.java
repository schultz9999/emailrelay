package com.example.emailrelay.queuing;

public interface ParcelSendHandler {
	void sendParcel(Parcel parcel);
}
