package com.example.emailrelay.resources;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.example.emailrelay.api.EmailEventStatistics;
import com.example.emailrelay.api.EmailResponse;
import com.example.emailrelay.persistence.EmailEventStore;
import com.google.inject.Inject;

@Path("/emailevent")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class EmailEventResource {
	
	private final EmailEventStore store;
	
	@Inject
	public EmailEventResource(EmailEventStore store) {
		this.store = store;
    }

    @POST
    @Timed
    public EmailResponse post(@NotNull List<Map<String, String>> request) {
    	if (request.size() > 0) {
        	store.persist(request);
    	}
    	
		return new EmailResponse(0, "OK");
    }
    
    @GET
    @Timed
    public EmailEventStatistics get() {
    	return store.stats();
    }
}
