package com.example.emailrelay.resources;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.example.emailrelay.api.Email;
import com.example.emailrelay.api.EmailResponse;
import com.example.emailrelay.queuing.Parcel;
import com.example.emailrelay.queuing.ParcelSendHandler;
import com.google.inject.Inject;

@Path("/email")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class EmailResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailResource.class);
	
	private final ParcelSendHandler sendHandler;
	
	@Inject
	public EmailResource(ParcelSendHandler sendHandler) {
		this.sendHandler = sendHandler;
    }

    @POST
    @Timed
    public EmailResponse post(@Valid Email email) {
    	
    	LOGGER.debug(email.toString());
    	
    	sendHandler.sendParcel(new Parcel(email));
    	
    	return new EmailResponse(0, "OK");
    }
}
