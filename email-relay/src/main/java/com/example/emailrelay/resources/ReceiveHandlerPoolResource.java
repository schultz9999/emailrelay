package com.example.emailrelay.resources;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.example.emailrelay.api.PoolSize;
import com.example.emailrelay.queuing.ParcelReceiveHandlerPool;
import com.google.inject.Inject;

@Path("/receivepool")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class ReceiveHandlerPoolResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReceiveHandlerPoolResource.class);

	private final ParcelReceiveHandlerPool receiveHandlerPool;

	@Inject
	public ReceiveHandlerPoolResource(ParcelReceiveHandlerPool receiveHandlerPool) {
		this.receiveHandlerPool = receiveHandlerPool;
	}

	@POST
	@Timed
	public PoolSize post(@Valid PoolSize poolSize) {
		LOGGER.debug("Resizing receiver pool to {}...", poolSize.getSize());
		this.receiveHandlerPool.resize(poolSize.getSize());
		return new PoolSize(receiveHandlerPool.size());
	}

	@GET
	public PoolSize get() {
		return new PoolSize(receiveHandlerPool.size());
	}
}
