package com.example.emailrelay.resources;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.example.emailrelay.forwarder.EmailForwarderManager;
import com.example.emailrelay.queuing.ParcelReceiveHandlerPool;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;

@Path("/stats")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class ReceiveStatisticsResource {

	private final ParcelReceiveHandlerPool receiveHandlerPool;
	private final EmailForwarderManager forwarderManager;

	@Inject
	public ReceiveStatisticsResource(ParcelReceiveHandlerPool receiveHandlerPool, EmailForwarderManager forwarderManager) {
		this.receiveHandlerPool = receiveHandlerPool;
		this.forwarderManager = forwarderManager;
	}

	@GET
	public Map<String, List<?>> get() {
		return ImmutableMap.of(
				"receiveStats", this.receiveHandlerPool.stats(),
				"forwardStats", this.forwarderManager.stats()
				);
	}
}
