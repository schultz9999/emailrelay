package com.example.emailrelay.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.example.emailrelay.api.Email;
import com.example.emailrelay.api.EmailSearchRequest;
import com.example.emailrelay.persistence.EmailStore;
import com.google.inject.Inject;

@Path("/search")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public final class SearchResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchResource.class);
	private final EmailStore store;
	
	@Inject
	public SearchResource(EmailStore store) {
		this.store = store;
    }

    @POST
    @Timed
    public List<Email> post(EmailSearchRequest request) {

    	if (request.getKeywords().size() == 0) {
    		return null;
    	}
    	
    	List<Email> result = store.search(request.getKeywords());
    	
    	LOGGER.debug("Matches: {}", result.size());
    	
    	return result;
    }
}
