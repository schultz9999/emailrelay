package com.example.emailrelay.filtering;

import static org.junit.Assert.*;

import org.junit.Test;

import com.example.emailrelay.api.Email;

public class EmailFilterTest {

	@Test
	public void testFilterNoLeadingAndTrailingSpaces() {
		HtmlTagsEmailBodyFilter target = new HtmlTagsEmailBodyFilter(null);
		Email email = new Email(null, null, null, null, null, "<h1>blah</h1>");
		target.filter(email);
		assertEquals("blah", email.getBody());
	}
	
	@Test
	public void testFilterSpacesInline() {
		HtmlTagsEmailBodyFilter target = new HtmlTagsEmailBodyFilter(null);
		Email email = new Email(null, null, null, null, null, "<h1><table><tr><td>a</td><td>b</td><td/></tr></table></h1>");
		target.filter(email);
		assertEquals("a b", email.getBody());
	}
	
	
	private static class UppercaseFilter extends EmailFilterBase {

		protected UppercaseFilter(EmailFilter next) {
			super(next);
		}

		@Override
		protected void apply(Email email) {
			email.setBody(email.getBody().toUpperCase());
		}
	}
	
	@Test
	public void testDecorationChain() {
		EmailFilter target = new HtmlTagsEmailBodyFilter(new UppercaseFilter(null));
		Email email = new Email(null, null, null, null, null, "<h1><table><tr><td>a</td><td>b</td><td/></tr></table></h1>");
		target.filter(email);
		assertEquals("A B", email.getBody());
	}
}
