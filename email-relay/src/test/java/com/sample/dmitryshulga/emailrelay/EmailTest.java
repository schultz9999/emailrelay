package com.sample.dmitryshulga.emailrelay;

import static org.junit.Assert.assertEquals;
import io.dropwizard.jackson.Jackson;

import org.junit.Test;

import com.example.emailrelay.api.Email;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EmailTest {

	@Test
	public void testRoundTripSerialization() throws Exception {
		
		final ObjectMapper om = Jackson.newObjectMapper();
		
		final Email expected = new Email("test@test.com", "test", "target@test.com", "target", "Test email subject", "<h1>Title</h1>");
		
		String json = om.writerWithType(Email.class).writeValueAsString(expected);
		
		final Email actual = om.readValue(json, new TypeReference<Email>() {});
		
		assertEquals(expected.getFrom(), actual.getFrom());
		assertEquals(expected.getFromName(), actual.getFromName());
		assertEquals(expected.getTo(), actual.getTo());
		assertEquals(expected.getToName(), actual.getToName());
		assertEquals(expected.getSubject(), actual.getSubject());
		assertEquals(expected.getBody(), actual.getBody());
	}
}
